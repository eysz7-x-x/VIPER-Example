//
//  TakeOverviewTest.swift
//  VIPER-Example
//
//  Created by Lukáš Tesař on 08.05.17.
//  Copyright © 2017 STRV. All rights reserved.
//

import XCTest
@testable import VIPER_Example

class TakeOverviewWireframeTest: XCTestCase {
    
    private(set) var takeOverviewWireframe: TakeOverviewWireframe!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        takeOverviewWireframe = TakeOverviewWireframe(withArtwork: Artwork(name: "Test artwork", id: "1"))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitCropView() {
        takeOverviewWireframe.testCropView(withImage: #imageLiteral(resourceName: "MonaLisa"))
        XCTAssertNotNil(takeOverviewWireframe.cropViewController?.overviewImage)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
