//
//  ResetPasswordTest.swift
//  VIPER-Example
//
//  Created by Lukáš Tesař on 08.05.17.
//  Copyright © 2017 STRV. All rights reserved.
//

import XCTest
@testable import VIPER_Example

class ResetPasswordInteractorTest: XCTestCase {
    
    private(set) var resetPasswordInteractor: ResetPasswordInteractor!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        resetPasswordInteractor = ResetPasswordInteractor()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testResetPassword() {
        resetPasswordInteractor.resetPassword(forEmail: "test@test.com")
        XCTAssertTrue(resetPasswordInteractor.didCallReset)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
