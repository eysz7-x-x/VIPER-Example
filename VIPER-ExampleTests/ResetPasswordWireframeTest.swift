//
//  ResetPasswordWireframeTest.swift
//  VIPER-Example
//
//  Created by Lukáš Tesař on 16.05.17.
//  Copyright © 2017 STRV. All rights reserved.
//

import XCTest
@testable import VIPER_Example

class ResetPasswordWireframeTest: XCTestCase {
    
    private(set) var resetPasswordWireframe: ResetPasswordWireframe!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        resetPasswordWireframe = ResetPasswordWireframe()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testThatResetPasswordViewControllerExists() {
        
        XCTAssertNotNil(resetPasswordWireframe.viewController, "Reset password view controller instance should be creatable from storyboard")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
