//
//  AppDelegate.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 06/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        // Here can be logic for detecting app state and deciding what root should start
        MainWireframe().makeRoot()
        return true
    }
}

