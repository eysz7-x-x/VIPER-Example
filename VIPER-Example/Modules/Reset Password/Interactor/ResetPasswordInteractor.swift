//
//  ResetPasswordInteractor.swift
//  oneprove-ios
//

import Foundation

class ResetPasswordInteractor {
    weak var presenter: ResetPasswordPresenterInterface?
    
    var didCallReset = false
}

extension ResetPasswordInteractor: ResetPasswordInteractorInterface {
    func resetPassword(forEmail email: String) {
        
        didCallReset = true
        
        RESTManager.resetPassword(forEmail: email) { [weak self] (data, error) in
            if let error = error {
                debugPrint("Reset Password failed: \(error)")
                self?.presenter?.resetRequestFailed()
            } else {
                self?.presenter?.resetRequestFinished()
            }
        }
    }
}
