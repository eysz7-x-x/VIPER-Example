//
//  ResetPasswordInteractorInterface.swift
//  oneprove-ios
//

import Foundation

protocol ResetPasswordInteractorInterface: class {
    func resetPassword(forEmail email: String)
}
