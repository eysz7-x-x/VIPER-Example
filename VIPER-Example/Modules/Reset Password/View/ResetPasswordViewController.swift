//
//  ResetPasswordViewController.swift
//  oneprove-ios
//

import UIKit

class ResetPasswordViewController: UIViewController {
    var presenter: ResetPasswordPresenterInterface?

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reset Password"
    }
    
    @IBAction func resetPasswordTapped() {
        presenter?.resetPasswordTapped()
    }
}

extension ResetPasswordViewController: ResetPasswordViewInterface {
    var emailString: String? {
        return emailTextField.text
    }
}

extension ResetPasswordViewController: ViewProtocol {
    func showFailure(withMessage message: String) {
        // Show failure alert here
        debugPrint("Showing failure: \(message)")
    }
    
    func showSuccess(withMessage message: String) {
        // Show Success alert here
        debugPrint("Showing success: \(message)")
    }
}

