//
//  ResetPasswordViewInterface.swift
//  oneprove-ios
//

import Foundation

protocol ResetPasswordViewInterface: ViewProtocol {
    var emailString: String? { get }
}
