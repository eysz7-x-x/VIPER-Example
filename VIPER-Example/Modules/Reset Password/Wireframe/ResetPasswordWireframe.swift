//
//  ResetPasswordWireframe.swift
//  oneprove-ios
//

import UIKit

class ResetPasswordWireframe {

    weak var viewController: ResetPasswordViewController?

    init() {
        viewController = loadControllerFromSB()
        let interactor = ResetPasswordInteractor()
        let presenter = ResetPasswordPresenter()
        presenter.wireframe = self
        presenter.view = viewController
        presenter.interactor = interactor
        viewController?.presenter = presenter
        interactor.presenter = presenter
    }
    
    fileprivate func loadControllerFromSB() -> ResetPasswordViewController {
        let storyboard = UIStoryboard.init(name: "ResetPassword", bundle: nil)
        let controller = storyboard.instantiateInitialViewController() as! ResetPasswordViewController
        return controller
    }
}

extension ResetPasswordWireframe: WireframeProtocol {
    func push() {
        guard let window = UIApplication.shared.keyWindow,
        let root = window.rootViewController as? UINavigationController,
        let vc = viewController
        else { return }
        root.pushViewController(vc, animated: true)
    }
    
    func pop() {
        guard let viewController = viewController else { return }
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func present() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController,
            let viewController = viewController
            else { return }
        root.present(viewController, animated: true, completion: nil)
    }
    
    func makeRoot() {
        guard let window = UIApplication.shared.keyWindow
        else { return }
        window.rootViewController = viewController
    }
}

extension ResetPasswordWireframe: ResetPasswordWireframeInterface {}
