//
//  ResetPasswordPresenterInterface.swift
//  oneprove-ios
//

import Foundation

protocol ResetPasswordPresenterInterface: PresenterProtocol {
    // MARK: - View
    func resetPasswordTapped()
    // MARK: - Interactor
    func resetRequestFinished()
    func resetRequestFailed()
}
