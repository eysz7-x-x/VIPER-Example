//
//  ResetPasswordPresenter.swift
//  oneprove-ios
//

import Foundation

class ResetPasswordPresenter {
    var interactor: ResetPasswordInteractorInterface?
    var wireframe: ResetPasswordWireframeInterface?
    weak var view: ResetPasswordViewInterface?
}

extension ResetPasswordPresenter: ResetPasswordPresenterInterface {
    // MARK: - Presenter Protocol
    func viewDidAppear() {}
    
    // MARK: - View
    func resetPasswordTapped() {
        guard let email = view?.emailString else {
            view?.showFailure(withMessage: "Please fill email address")
            return
        }
        
        guard email.characters.count > 0 else {
            view?.showFailure(withMessage: "Please fill email address")
            return
        }
        
        interactor?.resetPassword(forEmail: email)
    }
    // MARK: - Interactor
    func resetRequestFinished() {
        wireframe?.pop()
    }
    
    func resetRequestFailed() {
        view?.showFailure(withMessage: "Reseting Password failed")
    }
}
