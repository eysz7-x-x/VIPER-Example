//
//  ResetPasswordPresenter.swift
//  oneprove-ios
//

import UIKit

class MainPresenter {
    var interactor: MainInteractorInterface?
    var wireframe: MainWireframeInterface?
    weak var view: MainViewInterface?
}

extension MainPresenter: MainPresenterInterface {
    // MARK: - Presenter Protocol
    func viewDidAppear() {}

    // MARK: - View
    func resetButtonTapped() {
        wireframe?.pushResetPassword()
    }
    
    func menuButtonTapped() {
        wireframe?.presentMenu()
    }
    
    func takeOverviewButtonTapped() {
        let artwork = Artwork.init(name: "Mona Lisa", id: "1753716")
        wireframe?.presentTakeOverview(withArtwork: artwork)
    }
    
    func swipeRecognized(withPan pan: UIScreenEdgePanGestureRecognizer) {
        let translation = pan.translation(in: pan.view!)
        let movementOnAxis = translation.x / pan.view!.bounds.width
        let positiveMovementOnAxis = fmaxf(Float(movementOnAxis), 0.0)
        let positiveMovementOnAxisPercent = fminf(positiveMovementOnAxis, 1.0)
        let progress = CGFloat(positiveMovementOnAxisPercent)

        switch pan.state {
        case .began:
            wireframe?.presentMenuInteractive()
        case .changed:
            wireframe?.updateInteraction(withProgress: progress)
        case .cancelled:
            wireframe?.cancelInteraction()
        case .ended:
            progress > 0.3 ? wireframe?.finishInteraction():wireframe?.cancelInteraction()
        default:
            break
        }
    }
}
