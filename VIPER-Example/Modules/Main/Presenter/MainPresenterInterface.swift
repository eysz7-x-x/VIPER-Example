//
//  ResetPasswordPresenterInterface.swift
//  oneprove-ios
//

import UIKit

protocol MainPresenterInterface: PresenterProtocol {
    func resetButtonTapped()
    func takeOverviewButtonTapped()
    func menuButtonTapped()
    func swipeRecognized(withPan pan: UIScreenEdgePanGestureRecognizer)
}
