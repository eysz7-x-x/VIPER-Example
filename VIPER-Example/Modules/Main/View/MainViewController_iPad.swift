//
//  MainViewController-iPad.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 07/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import UIKit

class MainViewController_iPad: MainViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Main iPad"
    }
}

// MARK: - ViewProtocol
extension MainViewController_iPad {
    override func showFailure(withMessage message: String) {
        // Show failure alert here
        debugPrint("Showing failure: \(message)")
    }
    
    override func showSuccess(withMessage message: String) {
        // Show Success alert here
        debugPrint("Showing success: \(message)")
    }
}
