//
//  ResetPasswordViewController.swift
//  oneprove-ios
//

import UIKit

class MainViewController: UIViewController {
    var presenter: MainPresenterInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        presenter?.resetButtonTapped()
    }
    
    @IBAction func takeOverviewButtonTapped(_ sender: UIButton) {
        presenter?.takeOverviewButtonTapped()
    }
    
    func menuButtonTapped() {
        presenter?.menuButtonTapped()
    }
    
    func handleGesture(withPan pan: UIScreenEdgePanGestureRecognizer) {
        presenter?.swipeRecognized(withPan: pan)
    }
}

extension MainViewController: MainViewInterface {}

extension MainViewController: ViewProtocol {
    func showFailure(withMessage message: String) {
        // Show failure alert here
        debugPrint("Showing failure: \(message)")
    }
    
    func showSuccess(withMessage message: String) {
        // Show Success alert here
        debugPrint("Showing success: \(message)")
    }
}

fileprivate extension MainViewController {
    func setupLayout() {
        title = "Main"
        let barItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.reply, target: self, action: #selector(menuButtonTapped))
        
        navigationItem.leftBarButtonItem = barItem
        
        let enterPanGesture = UIScreenEdgePanGestureRecognizer.init(target: self, action: #selector(handleGesture(withPan:)))
        enterPanGesture.edges = UIRectEdge.left
        enterPanGesture.cancelsTouchesInView = true
        enterPanGesture.delaysTouchesEnded = true
        enterPanGesture.isEnabled = true
        view.addGestureRecognizer(enterPanGesture)
    }
}

