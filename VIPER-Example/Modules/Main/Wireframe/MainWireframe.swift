//
//  ResetPasswordWireframe.swift
//  oneprove-ios
//

import UIKit

class MainWireframe {
    weak var viewController: MainViewController?
    fileprivate weak var menuController: MenuWireframe?
    
    init() {
        viewController = loadControllerFromSB()
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        presenter.wireframe = self
        presenter.view = viewController
        presenter.interactor = interactor
        viewController?.presenter = presenter
        interactor.presenter = presenter
    }
    
    fileprivate func loadControllerFromSB() -> MainViewController? {
        // For multiple idioms support in one module, just instantiate correct controller from storyboard
        let name = UIDevice.current.userInterfaceIdiom == .pad ? "Main-iPad":"Main"
        let storyboard = UIStoryboard.init(name: name, bundle: nil)
        let controller = storyboard.instantiateInitialViewController() as? MainViewController
        return controller
    }
}

extension MainWireframe: WireframeProtocol {
    func push() {
        guard let window = UIApplication.shared.keyWindow,
        let root = window.rootViewController as? UINavigationController,
        let vc = viewController
        else { return }
        root.pushViewController(vc, animated: true)
    }
    
    func pop() {
        guard let viewController = viewController else { return }
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func present() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController,
            let viewController = viewController
            else { return }
        root.present(viewController, animated: true, completion: nil)
    }
    
    func makeRoot() {
        if (UIApplication.shared.delegate as! AppDelegate).window == nil {
            let window = UIWindow.init(frame: UIScreen.main.bounds)
            (UIApplication.shared.delegate as! AppDelegate).window = window
        }
        
        let window = (UIApplication.shared.delegate as! AppDelegate).window!
        guard let viewController = viewController else { return }
        let navController = UINavigationController.init(rootViewController: viewController)
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }
}

extension MainWireframe: MainWireframeInterface {
    func pushResetPassword() {
        let wireframe = ResetPasswordWireframe()
        wireframe.push()
    }
    
    func presentMenu() {
        let wireframe = MenuWireframe.init()
        wireframe.present()
    }
    
    func presentMenuInteractive() {
        if menuController == nil {
            debugPrint("creating menu")
            menuController = MenuWireframe.init()
        }
        
        menuController?.presentInteractive()
    }
    
    func presentTakeOverview(withArtwork artwork: Artwork) {
        let wireframe = TakeOverviewWireframe.init(withArtwork: artwork)
        wireframe.present()
    }

    func updateInteraction(withProgress progress: CGFloat) {
        menuController?.updateInteraction(withProgress: progress)
    }
    
    func cancelInteraction() {
        menuController?.cancelInteraction()
        menuController = nil
    }
    
    func finishInteraction() {
        menuController?.finishInteraction()
        menuController = nil
    }
}
