//
//  ResetPasswordWireframeInterface.swift
//  oneprove-ios
//

import UIKit

protocol MainWireframeInterface: WireframeProtocol {
    func pushResetPassword()
    func presentMenu()
    func presentMenuInteractive()
    func presentTakeOverview(withArtwork artwork: Artwork)
    func updateInteraction(withProgress progress: CGFloat)
    func cancelInteraction()
    func finishInteraction()
}
