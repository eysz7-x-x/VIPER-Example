//
//  TakeOverviewPresenterInterface.swift
//  oneprove-ios
//

import Foundation
import UIKit

protocol TakeOverviewPresenterInterface: PresenterProtocol {
    // MARK: - User Actions
    func takePhotoButtonPressed()
    func doneButtonTapped()
    func dismissTapped()
    // MARK: - Getters
    var baseImage: UIImage { get }
    var zoomImage: UIImage { get }
}
