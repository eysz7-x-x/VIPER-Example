//
//  TakeOverviewPresenter.swift
//  oneprove-ios
//

import Foundation
import UIKit

class TakeOverviewPresenter: NSObject {
    // MARK: - VIPER
    var interactor: TakeOverviewInteractorInterface?
    var wireframe: TakeOverviewWireframeInterface?
    weak var takeOverview: TakeOverviewViewInterface?
    weak var cropView: CropViewInterface?
}

extension TakeOverviewPresenter: TakeOverviewPresenterInterface {
    func viewDidAppear() {}
    
    func takePhotoButtonPressed() {
        if let takenImage = takeOverview?.takenImage {
            wireframe?.pushCropView(withImage: takenImage)
        }
    }
    
    func doneButtonTapped() {
        wireframe?.dismiss()
    }
    
    func dismissTapped() {
        wireframe?.dismiss()
    }
    
    // MARK: - Getters    
    var baseImage: UIImage {
        return #imageLiteral(resourceName: "MonaLisa")
    }
    
    var zoomImage: UIImage {
        return #imageLiteral(resourceName: "MonaLisaZoom")
    }
}
