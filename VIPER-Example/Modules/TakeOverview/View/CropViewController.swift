//
//  CropViewController.swift
//  oneprove-ios
//
//  Created by Lukáš Tesař on 01.02.17.
//  Copyright © 2017 Oneprove. All rights reserved.
//

import UIKit

class CropViewController: UIViewController {
    // MARK: - VIPER
    var presenter: TakeOverviewPresenterInterface?
    
    // MARK: - Variables
    fileprivate var sourceImageView: UIImageView?
    fileprivate var croppedImage: UIImage?
    var overviewImage: UIImage?
    fileprivate var initialRect: CGRect = .zero
    fileprivate var finalRect: CGRect = .zero
    fileprivate let cameraToolBarHeight: CGFloat = 120.0
    fileprivate let navBarHeight: CGFloat = 80.0
    fileprivate var cropped: Bool = false
    
    // MARK: - Outlets
    @IBOutlet weak var actionButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }
    
    // MARK: - User Actions
    @IBAction func actionButtonPressed(_ sender: UIButton) {
        if cropped {
            dismiss()
        } else {
            crop()
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        presenter?.doneButtonTapped()
    }
    
    func crop() {
        guard let presenter = presenter else { return }
        updateActionButtonAccesibilityToCrop(enable: false)
        
        let croppedImage = presenter.zoomImage
        UIView.transition(with: sourceImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.sourceImageView!.image = croppedImage
            self.croppedImage = croppedImage
        })
    }
    
    func dismiss() {
        presenter?.doneButtonTapped()
    }
}

extension CropViewController: CropViewInterface {
    var cropImage: UIImage? {
        return self.croppedImage
    }
}

extension CropViewController: ViewProtocol {
    func showFailure(withMessage message: String) {
        // Show failure alert here
        debugPrint("Showing failure: \(message)")
    }
    
    func showSuccess(withMessage message: String) {
        // Show Success alert here
        debugPrint("Showing success: \(message)")
    }
}

fileprivate extension CropViewController {
    func setupLayout() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        initSourceImageView()
        setCroppedImageFrames()
        
        updateActionButtonAccesibilityToCrop(enable: true)
    }
    
    func initSourceImageView() {
        sourceImageView = UIImageView.init(frame: CGRect(x: 0, y: navBarHeight, width: self.view.bounds.size.width, height: self.view.bounds.size.height - cameraToolBarHeight - navBarHeight))
        sourceImageView?.contentMode = .scaleAspectFit
        sourceImageView?.clipsToBounds = true
        view.addSubview(sourceImageView!)
        if let overviewImage = overviewImage {
            sourceImageView?.image = overviewImage
        }
    }
    
    func setCroppedImageFrames() {
        initialRect = sourceImageView!.frame
        finalRect = sourceImageView!.frame
    }
    
    func updateActionButtonAccesibilityToCrop(enable: Bool) {
        if enable {
            actionButton.setTitle("CHECK & CROP", for: .normal)
            cropped = false
        } else {
            actionButton.setTitle("CONTINUE", for: .normal)
            cropped = true
        }
    }
}
