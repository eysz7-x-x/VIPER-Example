//
//  CropViewInterface.swift
//  oneprove-ios
//
//  Created by Lukáš Tesař on 02.02.17.
//  Copyright © 2017 Oneprove. All rights reserved.
//

import Foundation
import UIKit

protocol CropViewInterface: ViewProtocol {
    // MARK: - Getters
    var cropImage: UIImage? { get }
}
