//
//  TakeOverviewViewController.swift
//  oneprove-ios
//

import UIKit
import AVFoundation

class TakeOverviewViewController: UIViewController {
    // MARK: - VIPER
    var presenter: TakeOverviewPresenterInterface?

    // MARK: - IBOutlets
    @IBOutlet weak var cameraPreview: UIImageView!
    @IBOutlet weak var takePhotoButton: UIButton!
    
    // MARK: - Variables
    fileprivate var image: UIImage? 
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
        loadBaseImage()
    }
    
    // MARK: - Actions
    @IBAction func takePhotoButtonPressed(_ sender: UIButton) {
        saveToCamera()
    }
    
    func dismissTapped() {
        presenter?.dismissTapped()
    }
}

extension TakeOverviewViewController: TakeOverviewViewInterface {
    var takenImage: UIImage? {
        return cameraPreview.image
    }
}

extension TakeOverviewViewController: ViewProtocol {
    func showFailure(withMessage message: String) {
        // Show failure alert here
        debugPrint("Showing failure: \(message)")
    }
    
    func showSuccess(withMessage message: String) {
        // Show Success alert here
        debugPrint("Showing success: \(message)")
    }
}

fileprivate extension TakeOverviewViewController {
    // MARK: - Private Methods
    func setupLayout() {
        let dismiss = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(dismissTapped))
        navigationItem.leftBarButtonItem = dismiss
    }
    
    func loadBaseImage() {
        guard let presenter = presenter else { return }
        cameraPreview.image = presenter.baseImage
    }
    
    func saveToCamera() {
        presenter?.takePhotoButtonPressed()
    }
}
