//
//  TakeOverviewViewInterface.swift
//  oneprove-ios
//

import Foundation
import UIKit

protocol TakeOverviewViewInterface: ViewProtocol {
    // MARK: - Getters
    var takenImage: UIImage? { get }
}
