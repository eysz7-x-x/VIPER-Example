//
//  TakeOverviewInteractor.swift
//  oneprove-ios
//

import Foundation

class TakeOverviewInteractor {
    weak var presenter: TakeOverviewPresenterInterface?
    var artwork: Artwork?
}

extension TakeOverviewInteractor: TakeOverviewInteractorInterface {
}
