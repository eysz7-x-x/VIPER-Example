//
//  TakeOverviewWireframe.swift
//  oneprove-ios
//

import UIKit

class TakeOverviewWireframe {

    weak var takeOverviewController: TakeOverviewViewController?
    weak var cropViewController: CropViewController?
    // Exception to store a presenter here
    weak var presenter: TakeOverviewPresenter?
    
    init(withArtwork artwork: Artwork) {
        takeOverviewController = loadTakeOverviewControllerFromSB()
        let interactor = TakeOverviewInteractor()
        let presenter = TakeOverviewPresenter()
        presenter.wireframe = self
        presenter.takeOverview = takeOverviewController
        presenter.interactor = interactor
        takeOverviewController?.presenter = presenter
        interactor.presenter = presenter
        interactor.artwork = artwork
        self.presenter = presenter
    }
    
    fileprivate func loadTakeOverviewControllerFromSB() -> TakeOverviewViewController {
        let storyboard = UIStoryboard.init(name: "TakeOverview", bundle: nil)
        let controller = storyboard.instantiateInitialViewController() as! TakeOverviewViewController
        return controller
    }
    
    fileprivate func loadCropViewControllerFromSB() -> CropViewController {
        let storyboard = UIStoryboard.init(name: "TakeOverview", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CropViewController") as! CropViewController
        return controller
    }
    
    // MARK: - testing
    func testCropView(withImage image: UIImage) {
        let crop = loadCropViewControllerFromSB()
        crop.overviewImage = image
        crop.presenter = presenter
        presenter?.cropView = crop
        crop.navigationItem.hidesBackButton = false
        cropViewController = crop
    }
}

extension TakeOverviewWireframe: TakeOverviewWireframeInterface {
    func push() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController as? UINavigationController,
            let vc = takeOverviewController
            else { return }
        root.pushViewController(vc, animated: true)
    }
    
    func present() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController,
            let viewController = takeOverviewController
            else { return }
        
        let navController = UINavigationController(rootViewController: viewController)        
        root.present(navController, animated: true, completion: nil)
    }
    
    func pushCropView(withImage image: UIImage) {
        guard let rootNav = takeOverviewController?.navigationController else { return }
        
        let crop = loadCropViewControllerFromSB()
        crop.overviewImage = image
        crop.presenter = presenter
        presenter?.cropView = crop
        crop.navigationItem.hidesBackButton = false
        cropViewController = crop
        rootNav.viewControllers.last?.navigationItem.title = " "
        DispatchQueue.main.async {
            rootNav.pushViewController(crop, animated: true)
        }
    }
    
    func pop() {
        guard let viewController = cropViewController else { return }
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func makeRoot() {
        if (UIApplication.shared.delegate as! AppDelegate).window == nil {
            let window = UIWindow.init(frame: UIScreen.main.bounds)
            (UIApplication.shared.delegate as! AppDelegate).window = window
        }
        
        let window = (UIApplication.shared.delegate as! AppDelegate).window!
        guard let viewController = takeOverviewController else { return }
        let navController = UINavigationController.init(rootViewController: viewController)
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }
    
    func dismiss() {
        if let crop = cropViewController {
            crop.dismiss(animated: true, completion: nil)
        } else {
            takeOverviewController?.dismiss(animated: true, completion: nil)
        }
    }
}
