//
//  TakeOverviewWireframeInterface.swift
//  oneprove-ios
//

import Foundation
import UIKit

protocol TakeOverviewWireframeInterface: WireframeProtocol {
    /// Pushes this wireframe's viewController on current Navigation Stack
    func pushCropView(withImage image: UIImage)
}
