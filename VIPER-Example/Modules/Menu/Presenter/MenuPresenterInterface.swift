//
//  MenuPresenterInterface.swift
//  oneprove-ios
//

import UIKit

protocol MenuPresenterInterface: PresenterProtocol {
    func dismiss()
    func panGestureRecognized(withGesture gesture: UIPanGestureRecognizer)
}
