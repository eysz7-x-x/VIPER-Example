//
//  MenuPresenter.swift
//  oneprove-ios
//

import Foundation
import MessageUI

class MenuPresenter {
    var interactor: MenuInteractorInterface?
    var wireframe: MenuWireframeInterface?
    weak var view: MenuViewInterface?
}

extension MenuPresenter: MenuPresenterInterface {
    func viewDidAppear() {}
    
    func dismiss() {
        wireframe?.dismiss()
    }
    
    // Presenter knows about when to present viewController
    // Wireframe knows how to
    func panGestureRecognized(withGesture gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: gesture.view!)
        let movementOnAxis = translation.x / gesture.view!.bounds.width
        let positiveMovementOnAxis = fmaxf(Float(abs(movementOnAxis)), 0.0)
        let positiveMovementOnAxisPercent = fminf(positiveMovementOnAxis, 1.0)
        let progress = CGFloat(positiveMovementOnAxisPercent)

        switch gesture.state {
        case .began:
            wireframe?.dismissInteractive()
        case .changed:
            wireframe?.updateInteraction(withProgress: progress)
        case .cancelled:
            wireframe?.cancelInteraction()
        case .ended:
            progress > 0.3 ? wireframe?.finishInteraction():wireframe?.cancelInteraction()
        default:
            break
        }
    }
}
