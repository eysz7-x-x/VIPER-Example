//
//  MenuWireframeInterface.swift
//  oneprove-ios
//

import UIKit

@objc protocol MenuWireframeInterface: WireframeProtocol {
    func presentInteractive()
    func dismissInteractive()
    func updateInteraction(withProgress progress: CGFloat)
    func cancelInteraction()
    func finishInteraction()
}
