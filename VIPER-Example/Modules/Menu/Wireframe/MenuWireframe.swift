//
//  MenuWireframe.swift
//  oneprove-ios
//

import UIKit

class MenuWireframe: NSObject {

    weak var viewController: MenuViewController?
    
    fileprivate var interactor: InteractionManager?
    
    override init() {
        super.init()
        viewController = loadControllerFromSB()
        let interactor = MenuInteractor()
        let presenter = MenuPresenter()
        presenter.wireframe = self
        presenter.view = viewController
        presenter.interactor = interactor
        viewController?.presenter = presenter
        interactor.presenter = presenter
    }
    
    fileprivate func loadControllerFromSB() -> MenuViewController {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let controller = storyboard.instantiateInitialViewController() as! MenuViewController
        return controller
    }
}

extension MenuWireframe: MenuWireframeInterface {
    func push() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController as? UINavigationController,
            let vc = viewController
            else { return }
        root.pushViewController(vc, animated: true)
    }
    
    func pop() {
        guard let viewController = viewController else { return }
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func makeRoot() {
        if (UIApplication.shared.delegate as! AppDelegate).window == nil {
            let window = UIWindow.init(frame: UIScreen.main.bounds)
            (UIApplication.shared.delegate as! AppDelegate).window = window
        }
        
        let window = (UIApplication.shared.delegate as! AppDelegate).window!
        guard let viewController = viewController else { return }
        let navController = UINavigationController.init(rootViewController: viewController)
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }

    func present() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController,
            let vc = viewController
            else { return }
        interactor = InteractionManager()
        vc.transitioningDelegate = interactor
        root.present(vc, animated: true, completion: nil)
    }
    
    func presentInteractive() {
        guard let window = UIApplication.shared.keyWindow,
            let root = window.rootViewController,
            let vc = viewController
            else { return }
        interactor = InteractionManager()
        interactor?.hasStarted = true
        vc.transitioningDelegate = interactor
        root.present(vc, animated: true, completion: nil)
    }
    
    func updateInteraction(withProgress progress: CGFloat) {
        interactor?.update(progress)
    }

    func finishInteraction() {
        interactor?.finish()
    }
    
    func cancelInteraction() {
        interactor?.cancel()
    }
    
    func dismiss() {
        guard let viewController = viewController else { return }
        interactor = InteractionManager()
        viewController.transitioningDelegate = interactor
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func dismissInteractive() {
        guard let viewController = viewController else { return }
        interactor = InteractionManager()
        interactor?.hasStarted = true
        viewController.transitioningDelegate = interactor
        viewController.dismiss(animated: true, completion: nil)
    }
}
