//
//  MenuViewController.swift
//  oneprove-ios
//

import UIKit

class MenuViewController: UIViewController {
    var presenter: MenuPresenterInterface?

    @IBOutlet weak var dismissView: UIView!
    
    fileprivate var tapGesture: UITapGestureRecognizer?
    fileprivate var swipeGesture: UIPanGestureRecognizer?
    
    override func viewDidLoad() {
    	super.viewDidLoad()
        setupLayout()
    }
    
    func dismissMenu() {
        presenter?.dismiss()
    }
    
    func panGestureRecognized(withGesture gesture: UIPanGestureRecognizer) {
        presenter?.panGestureRecognized(withGesture: gesture)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        debugPrint("did disapear")
        dismissView.removeGestureRecognizer(tapGesture!)
        view.removeGestureRecognizer(swipeGesture!)
    }
}

extension MenuViewController: MenuViewInterface {
    func showFailure(withMessage message: String) {}
    func showSuccess(withMessage message: String) {}
}

fileprivate extension MenuViewController {
    func setupLayout() {
        tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(dismissMenu))
        dismissView.addGestureRecognizer(tapGesture!)
        addSwipeGesture()
    }
    
    func addSwipeGesture() {
        swipeGesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGestureRecognized(withGesture:)))
        view.addGestureRecognizer(swipeGesture!)
    }
}
