//
//  Artwork.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 15/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation

struct Artwork {
    let name: String
    let id: String
}
