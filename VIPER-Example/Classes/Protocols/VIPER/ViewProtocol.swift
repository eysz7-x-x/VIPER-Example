//
//  ViewProtocol.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 06/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation

protocol ViewProtocol: class {
    func showFailure(withMessage message: String)
    func showSuccess(withMessage message: String)
}
