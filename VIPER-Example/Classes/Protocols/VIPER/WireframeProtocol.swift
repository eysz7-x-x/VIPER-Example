//
//  WireframeProtocol.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 06/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation

@objc protocol WireframeProtocol: class {
    // Present self on current Navigation Stack
    func present()
    // Push self on current Navigation Stack
    func push()
    // Dismiss self from current Navigation Stack
    func dismiss()
    // Pop self from current Navigation Stack
    func pop()
    // Make ViewController as Root for current Navigation Stack
    func makeRoot()
}
