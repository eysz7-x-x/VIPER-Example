//
//  PresenterProtocol.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 07/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation

protocol PresenterProtocol: class {
    func viewDidAppear()
}
