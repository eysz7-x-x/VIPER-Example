//
//  RESTService.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 06/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation
import Alamofire

typealias Completion = (_ data: Any?, _ error: Error?)->()

class RESTService {
    // MARK: - Router
    enum Router: URLRequestConvertible {
        // MARK: Cases
        case resetPassword(email: String)
        
        var baseLink: String {
            return "https://baselink.example.viper"
        }

        // MARK: Paths
        var path: String {
            switch self {
            case .resetPassword:
                return "\(baseLink)/reset"
            }
        }
        
        // MARK: Methods
        var method: Alamofire.HTTPMethod {
            return .post
        }
        
        // MARK: Parameters
        var parameters: [String:Any]? {
            switch self {
                case .resetPassword(let email):
                return ["email":email]
            }
        }
        
        // MARK: URLRequestConvertible
        func asURLRequest() throws -> URLRequest {
            var request = URLRequest.init(url: URL.init(string: "\(path)")!)
            request.httpMethod = method.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            return try Alamofire.JSONEncoding.default.encode(request, with: parameters)
        }
    }
    
    // MARK: - Interface
    func request(router: Router, completionHandler: @escaping Completion) {
        do {
            Alamofire.request(try router.asURLRequest())
                // Validate response, only 200..299 responses are result.success
                .validate()
                .responseJSON { data in
                    switch data.result {
                    case .success(let data):
                        completionHandler(data, nil)
                    case .failure(let error):
                        completionHandler(nil, error)
                    }
            }
        } catch let error {
            // Alamofire try-catch error
            completionHandler(nil, error)
        }
    }
}
