//
//  InteractionManager.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 07/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import UIKit

class InteractionManager: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
}

extension InteractionManager: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return hasStarted ? self:nil
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return hasStarted ? self:nil
    }
}

extension InteractionManager: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? MenuViewController {
            // Animate Menu
            animateMenu(withContext: transitionContext)
        } else {
            // Dismiss menu
            dismissMenu(withContext: transitionContext)
        }
    }
    
    private func animateMenu(withContext transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? MenuViewController else {
                transitionContext.completeTransition(false)
                return
        }
        
        let containerView = transitionContext.containerView
        containerView.addSubview(toVC.view)
        let snapshot = fromVC.view.snapshotView(afterScreenUpdates: false)!
        snapshot.isUserInteractionEnabled = false
        snapshot.layer.shadowOpacity = 0.7
        toVC.dismissView.addSubview(snapshot)
        
        let offScreenRight = CGAffineTransform(translationX: UIScreen.main.bounds.width * 0.8, y: 0)
        
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                toVC.dismissView.transform = offScreenRight
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
    
    private func dismissMenu(withContext transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? MenuViewController else {
            transitionContext.completeTransition(false)
            return
        }
        
        transitionContext.containerView.addSubview(fromVC.view)
        let offScreenRight = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       animations: {
                        fromVC.dismissView.transform = offScreenRight
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
