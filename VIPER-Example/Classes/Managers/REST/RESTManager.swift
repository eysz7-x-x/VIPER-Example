//
//  RESTManager.swift
//  VIPER-Example
//
//  Created by Martin Púčik on 06/05/2017.
//  Copyright © 2017 STRV. All rights reserved.
//

import Foundation

class RESTManager {
    
    fileprivate static let service = RESTService()
    
    static func resetPassword(forEmail email: String, completion: @escaping Completion) {
        service.request(router: RESTService.Router.resetPassword(email: email), completionHandler: completion)
    }
}
